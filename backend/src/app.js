import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import db from './bin/database'
import indexRouter from './routes/index';
import fetchCharactersRouter from './routes/fetchCharacters';
import charactersRouter from './routes/characters'



var app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'GET,OPTIONS')
    next();
});


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/', indexRouter);
app.use('/fetch-characters', fetchCharactersRouter);
app.use('/characters', charactersRouter);


app.use(express.static(path.join(__dirname, '../public')));

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("we're connected!")
});


export default app;
