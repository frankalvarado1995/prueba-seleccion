import mongoose from 'mongoose';

//Define a schema
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

var Characters = new Schema({
    charactersId: ObjectId,
    titles: [String],
    origin: [String],
    siblings: [String],
    spouse: [String],
    lovers: [String],
    culture: [String],
    religion: [String],
    allegiances: [String],
    seasons: [String],
    appearances: [String],
    name: String,
    slug: String,
    image: String,
    gender: String,
    alive: Boolean,
    father: String,
    house: String,
    first_seen: String,
    actor: String,
    related: [],
    age: 0
});

const CharactersModel = mongoose.model('Characters', Characters);

export default CharactersModel;
