import express from 'express';
import bodyParser from 'body-parser';
import CharactersModel from '../models/characters';
var router = express.Router();
var jsonParser = bodyParser.json()



router.get('/', jsonParser, async function (req, res) {
    const { query } = req;
    const { page = 0, search } = query;
    const skip = page > 1 ? (page - 1) * 10 : 0;
    const limit = page === 0 ? 0 : 10;
    let totalPages = 0;

    await CharactersModel.countDocuments({ $or: [{ 'name': new RegExp(search, 'i') }, { 'house': new RegExp(search, 'i') }] }, function (err, count) {
        if (err) {
            res.send({
                msj: 'ERROR',
                desc: err.errmsg,
            })
        }
        totalPages = Math.ceil(count / 10)
    });
    await CharactersModel.find({ $or: [{ 'name': new RegExp(search, 'i') }, { 'house': new RegExp(search, 'i') }] }, function (err, characters) {
        if (err) {
            res.send({
                msj: 'ERROR',
                desc: err.errmsg,
            });
        }
        res.send({ characters, totalPages });

    }).limit(limit).skip(skip);
});

router.get('/:id', jsonParser, function (req, res) {
    const { id } = req.params
    CharactersModel.findById(id, function (err, character) {
        if (err) {
            res.send({
                msj: 'ERROR',
                desc: err.errmsg,
            });
        }
        res.send(character);
    });
});

export default router;



