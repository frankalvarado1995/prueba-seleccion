import express from 'express';
import bodyParser from 'body-parser';
import request from 'request-promise';
import CharactersModel from '../models/characters';

var router = express.Router();
var jsonParser = bodyParser.json()
var URL = "https://api.got.show/api/show/characters";




router.get('/', jsonParser, function (req, res, next) {
    request({
        uri: URL,
        json: true,
    }).then(data => {
        console.log(data)
        CharactersModel.insertMany(data, function (err, characters) {
            if (err) {
                res.send({
                    msj: 'ERROR',
                    desc: err.errmsg,
                });
            }


            res.send(characters);
        })

    });

});

export default router;



