import mongoose from 'mongoose';

const mongoDB = 'mongodb://localhost/GOT'
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;


export default db;
