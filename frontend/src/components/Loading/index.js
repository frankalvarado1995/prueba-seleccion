import React from 'react'
import LoaderPredefined from './LoaderPredefined'

function Loading(props) {
  if (props.error) {
    return <div>Error! Please refresh the page</div>
  } else if (props.pastDelay) {
    return (
      <div>
        <LoaderPredefined />
      </div>
    )
  } else {
    return null // Avoiding Flash Of Loading Component (<200ms)
  }
}

export default Loading
