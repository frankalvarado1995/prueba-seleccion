import React from 'react'
import { Dimmer, Loader, Segment } from 'semantic-ui-react'

const LoaderPredefined = () => (
  <div>
    <Segment>
      <Dimmer active>
        <Loader>Loading</Loader>
      </Dimmer>
    </Segment>
  </div>
)

export default LoaderPredefined
