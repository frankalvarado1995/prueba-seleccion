import React from 'react'
import { isEmpty, debounce } from 'lodash'
import { Header, Image, Pagination, Grid, Segment, Search, Item } from 'semantic-ui-react'

class ListCharacters extends React.PureComponent {
  state = {
    characters: [],
    page: 1,
    totalPages: 0,
    isLoading: false,
    results: [],
    value: '',
  }

  componentDidMount() {
    const { page } = this.state
    this.fetchCharacters({ page })
  }

  fetchCharacters = params => {
    const { page, search = '' } = params
    fetch(`http://localhost:9000/characters?page=${page}&search=${search}`)
      .then(response => response.json())
      .then(data => {
        const { characters, totalPages } = data
        this.setState({ characters, totalPages })
      })
  }

  handlePaginationChange = (_, { activePage }) => {
    const { value } = this.state
    this.fetchCharacters({ page: activePage, search: value })
    this.setState({ page: activePage })
  }

  handleSearchChange = async (e, { value }) => {
    await this.setState({ isLoading: true, value })
    const { value: valueSearch } = this.state
    if (isEmpty(valueSearch)) return this.resetComponent()
    await this.fetchCharacters({ search: value })
    const { characters } = this.state
    const results = characters.map(result => {
      const { _id: id } = result
      return {
        image: result.image,
        title: result.name,
        id,
      }
    })
    await this.setState({
      isLoading: false,
      results,
    })
  }

  handleResultSelect = (_, { result }) => {
    window.location.href = `/details/${result.id}`
  }

  resetComponent = () => {
    this.fetchCharacters({ search: '', page: 1 })
    this.setState({ isLoading: false, results: [], value: '' })
  }

  render() {
    const { characters, page, totalPages, isLoading, results, value } = this.state
    return (
      <Grid centered columns={2} style={{ background: 'url(/list.jpg)' }}>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Header as="h2" icon textAlign="center">
                <Image src="/got.png" circular centered />
                <Header.Content>Lista de personajes</Header.Content>
              </Header>
              <Search
                loading={isLoading}
                onResultSelect={this.handleResultSelect}
                onSearchChange={debounce(this.handleSearchChange, 500, { leading: true })}
                results={results}
                value={value}
                {...this.props}
              />
              <Item.Group link>
                {characters.map(character => {
                  const { _id: id } = character
                  return (
                    <Item href={`/details/${id}`} as="a" key={`${character.name}-${Date.now()}`}>
                      <Item.Image size="mini" src={character.image} />

                      <Item.Content>
                        <Item.Header>{character.name}</Item.Header>
                        <Item.Description>{character.house}</Item.Description>
                      </Item.Content>
                    </Item>
                  )
                })}
              </Item.Group>

              <Pagination
                activePage={page}
                onPageChange={this.handlePaginationChange}
                totalPages={totalPages}
              />
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default ListCharacters
