import React from 'react'
import { Container, Header, Image, Grid, Segment, List, Button } from 'semantic-ui-react'

class Details extends React.PureComponent {
  state = {
    character: {},
  }

  componentDidMount() {
    const { match } = this.props
    const { params } = match
    const { id } = params
    this.fetchCharacterById(id)
  }

  fetchCharacterById = id => {
    fetch(`http://localhost:9000/characters/${id}`)
      .then(response => response.json())
      .then(character => {
        this.setState({ character })
      })
  }

  ItemList = ({ title, value }) => (
    <List.Item>
      <List.Content>
        <List.Header>{title}</List.Header>
        {value}
      </List.Content>
    </List.Item>
  )

  render() {
    const { character } = this.state
    return (
      <Segment style={{ padding: '10em', background: 'url(/backgroud.png)' }} vertical>
        <Button href="/" secondary>
          Volver
        </Button>

        <Grid columns="equal" stackable>
          <Grid.Row textAlign="center">
            <Grid.Column width={6} stretched>
              <Grid.Row>
                <Grid.Column>
                  <Image src={character.image} circular centered />
                  <Header
                    style={{ color: 'white', background: '#1a1b1c', borderRadius: '10px' }}
                    as="h2"
                  >
                    {character.name}
                  </Header>
                </Grid.Column>
              </Grid.Row>
            </Grid.Column>
            <Grid.Column>
              <Container text>
                <Segment inverted>
                  <List divided inverted relaxed>
                    <this.ItemList title="Actor" value={character.actor} />
                    <this.ItemList title="Genero" value={character.gender} />
                    <this.ItemList title="Primera Aparición" value={character.first_seen} />
                    <this.ItemList title="Casa" value={character.house} />
                    <this.ItemList title="Titulos" value={String(character.titles)} />
                    <this.ItemList title="Origen" value={String(character.origin)} />
                    <this.ItemList title="Padre" value={character.father} />
                    <this.ItemList title="Hermanos" value={String(character.siblings)} />
                    <this.ItemList title="Esposos" value={String(character.spouse)} />
                    <this.ItemList title="Amantes" value={String(character.lovers)} />
                    <this.ItemList
                      title="¿Aun vive en la serie?"
                      value={character.alive ? 'Si' : 'No'}
                    />
                    <this.ItemList title="Lealtades" value={String(character.allegiances)} />
                    <this.ItemList title="Apariciones" value={String(character.appearances)} />
                    <this.ItemList title="Cultura" value={String(character.culture)} />
                    <this.ItemList title="Religión" value={String(character.religion)} />
                    <this.ItemList title="Temporadas" value={String(character.seasons)} />
                    <this.ItemList title="Slug" value={character.slug} />
                  </List>
                </Segment>
              </Container>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    )
  }
}

export default Details
