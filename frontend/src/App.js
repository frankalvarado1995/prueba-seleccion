import React from 'react'
import './App.css'
import loadable from 'react-loadable'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import LoadingComponent from './components/Loading'

const ListPage = loadable({
  loader: () => import('./components/List'),
  loading: LoadingComponent,
})

const DetailsPage = loadable({
  loader: () => import('./components/Details'),
  loading: LoadingComponent,
})

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={ListPage} />
        <Route path="/details/:id" exact component={DetailsPage} />
      </Switch>
    </Router>
  )
}

export default App
